package com.desafio.custom.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmptyRiqueredField extends RuntimeException {

	private static final long serialVersionUID = 3790949477066968425L;
	
	private String errorCode;
	private String errorMensage;
}
