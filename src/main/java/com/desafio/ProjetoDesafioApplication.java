package com.desafio;

import java.util.Objects;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.desafio.configuracoes.security.AuditorAwareImpl;
import com.desafio.entity.Pessoa;
import com.desafio.enums.PerfilEnum;
import com.desafio.service.CustomUseDetailsManager;

@EnableJpaAuditing(auditorAwareRef = "auditorAware")
@SpringBootApplication
@EnableFeignClients
public class ProjetoDesafioApplication {

	@Bean
	public AuditorAware<String> auditorAware() {
		return new AuditorAwareImpl();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(ProjetoDesafioApplication.class, args);
	}
	
	@Bean
	CommandLineRunner init(CustomUseDetailsManager pessoaService, PasswordEncoder passEncoder) {
		return args -> {
			initAdministrador(pessoaService,passEncoder);
		}; 
	}
	
	private void initAdministrador(CustomUseDetailsManager pessoaService, PasswordEncoder passEncode) {
		Pessoa administrador = pessoaService.findByEmail("administrador@desafio.com");
		if (Objects.isNull(administrador)) {
		pessoaService.salvarPessoa("administrador@adhood.com", PerfilEnum.ADMIN, "611.000.640-87");
		}
		Pessoa operador = pessoaService.findByEmail("operador@desafio.com");
		if (Objects.isNull(operador)) {
			pessoaService.salvarPessoa("operador@desafio.com", PerfilEnum.OPERADOR, "325.661.030-70");
		}
		Pessoa usuario = pessoaService.findByEmail("usuario@desafio.com");
		if (Objects.isNull(usuario)) {
			pessoaService.salvarPessoa("usuario@desafio.com", PerfilEnum.USUARIO, "024.371.650-88");
		}
	}

}
